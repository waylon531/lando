//    lando, a book management tool
//    Copyright (C) 2018 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
use rocket_contrib::{Json,Template,Engines};
use rocket_contrib::tera;
use rocket;
use rocket::Rocket;
use rocket::http::Status;
use rocket::response::Failure;
use rocket::response::NamedFile;

use book::{Book,GoogleBook};
use book::{ISBN,BookId};
use schema::books::dsl::*;

use diesel;
use dotenv::dotenv;
use diesel::prelude::*;
use diesel::insert_into;
use diesel::pg::PgConnection;

use std::env;
use std::collections::HashMap;
use std::path::{PathBuf,Path};

use reqwest;


pub fn establish_connection() -> Result<PgConnection,Failure> {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .map_err(|_e| Failure(Status::new(500, "Error connecting to database")))
        //.expect(&format!("Error connecting to {}", database_url))
}

#[get("/")]
pub fn index() -> Result<Template,Failure> {
    let connection = establish_connection()?;

    //This could be a map_err
    let book_list: Vec<Book> = match books.load::<Book>(&connection) {
        Ok(k) => Ok(k),
        Err(_) => Err(Failure(Status::new(500, "Database error")))
    }?;

    let mut context = HashMap::new();
    context.insert("book_list", &book_list);

    Ok(Template::render("main", &context))

}

#[get("/books")]
pub fn get_books() -> Result<Json<Vec<Book>>,Failure> {
    let connection = establish_connection()?;
    match books.load::<Book>(&connection) {
        Ok(k) => Ok(Json(k)),
        Err(_) => Err(Failure(Status::new(500, "Database error")))
    }

}

#[get("/add_book/<book_number>/as/<name>")]
pub fn add_book(book_number: ISBN, name: String) -> Result<Json<Book>,Failure> {
    dotenv().ok();
    let api_key= env::var("API_KEY")
        .expect("API_KEY must be set");
    let url = if book_number >= 10000000000 {
        format!("https://www.googleapis.com/books/v1/volumes?q=isbn:{:013}&key={}",
                book_number,
                api_key)

    } else {
        format!("https://www.googleapis.com/books/v1/volumes?q=isbn:{:010}&key={}",
                book_number,
                api_key)

    };
    println!("{}",url);
    let book: GoogleBook = reqwest::get(
            &url
        ).map_err(|_e| Failure(Status::new(500, "Error connecting to google books api")))?
        .json()
        .map_err(|e| {println!("ERROR: {:?}",e);Failure(Status::new(500, "Invalid data from google books api"))})?;
    let book_title = book.items[0].volumeInfo.title.clone();
    let book_authors = book.items[0].volumeInfo.authors.clone();
    let book_description = book.items[0].volumeInfo.description.clone();
    let book_link = book.items[0].volumeInfo.infoLink.clone();
            
    let connection = establish_connection()?;
    match insert_into(books)
        .values((title.eq(book_title), isbn.eq(book_number),
                 authors.eq(book_authors),owned_by.eq(name),
                 info_link.eq(book_link),description.eq(book_description)))
        .get_results(&connection) {
            Ok(mut k) => Ok(Json(k.remove(0))),
            Err(_) => Err(Failure(Status::new(500, "Unable to insert book")))
        }
}

#[get("/remove_book/<book_id>")]
pub fn remove_book(book_id: BookId) -> Result<String,Failure> {
    let connection = establish_connection()?;
    match diesel::delete(books.filter(id.eq(book_id)))
        .execute(&connection) {
            Ok(_) => Ok(format!("Deleted book {}",book_id)),
            Err(_) => Err(Failure(Status::new(400, "Book not found")))
        }
}

#[get("/borrow/<book_id>/as/<name>")]
pub fn checkout(book_id: BookId, name: String) -> Result<Json<Book>,Failure> {
    let connection = establish_connection()?;
    match diesel::update(books.filter(id.eq(book_id)))
        .set(borrowed_by.eq(Some(name)))
        .get_result(&connection) {
            Ok(k) => Ok(Json(k)),
            //TODO: this might not be the only error
            Err(_) => Err(Failure(Status::new(400, "Book not found")))
        }
}

#[get("/return/<book_id>")]
pub fn return_book(book_id: BookId) -> Result<Json<Book>,Failure> {
    let connection = establish_connection()?;
    let none: Option<String> = None;
    match diesel::update(books.filter(id.eq(book_id)))
        .set(borrowed_by.eq(none))
        .get_result(&connection) {
            Ok(k) => Ok(Json(k)),
            //TODO: this might not be the only error
            Err(_) => Err(Failure(Status::new(400, "Book not found")))
        }
}

#[get("/static/<file..>")]
pub fn static_files(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/").join(file)).ok()
}

fn as_int(value: tera::Value, _: HashMap<String, tera::Value>) -> tera::Result<tera::Value> {
    Ok(
        match value {
            tera::Value::Number(n) => n.as_f64().unwrap_or(0.0) as usize,
            _ => 0usize
        }.into() 
    )

}
fn extend(value: tera::Value, _: HashMap<String, tera::Value>) -> tera::Result<tera::Value> {
        let num = match value {
            tera::Value::Number(n) => n.as_i64().unwrap_or(0),
            _ => 0i64
        };
        Ok(tera::Value::String(format!("{:010}",num)))

}

pub fn run() -> Rocket {
    let engine = |engines: &mut Engines| {
        engines.tera.register_filter("as_int", as_int);
        engines.tera.register_filter("extend", extend);
    };
    let template = Template::custom(engine);
    rocket::ignite().mount("/", routes![index,get_books,add_book,remove_book,checkout,return_book,static_files])
        .attach(template)
}
