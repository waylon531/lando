table! {
    books (id) {
        id -> Int4,
        title -> Varchar,
        isbn -> Int8,
        authors -> Array<Text>,
        owned_by -> Text,
        borrowed_by -> Nullable<Text>,
        info_link -> Text,
        description -> Text,
    }
}
