use book::Book;
use routes;

use rocket::local::Client;
use rocket::http::Status;

use serde_json;

macro_rules! get {
    ($uri: expr) => { {
        let rocket = routes::run();
        let client = Client::new(rocket).expect("valid rocket instance");
        let req = client.get($uri);
        let mut response = req.dispatch();
        assert_eq!(response.status(), Status::Ok);
        response.body_string().unwrap()
    }}
}

#[test]
fn get_books() {
    let response = get!("/books/");
    let _val: Vec<Book> = serde_json::from_str(&response).unwrap();
}
#[test]
fn add_and_delete_book() {
    let response = get!("/add_book/0143037528/as/test");
    let added_book: Book = serde_json::from_str(&response).unwrap();
    let response = get!("/books/");
    let books: Vec<Book> = serde_json::from_str(&response).unwrap();
    //Find the book with the right id
    let book = books.into_iter().find(|b| b.book_id == added_book.book_id).unwrap();
    assert_eq!(book,added_book);
    assert_eq!(book.owned_by.as_str(),"test");
    assert_eq!(book.title.as_str(),"The Art of War");
    let _response = get!(format!("/remove_book/{}",book.book_id));
}

#[test]
fn long_isbn_test() {
    let response = get!("/add_book/9780143037521/as/long_isbn_test");
    let added_book: Book = serde_json::from_str(&response).unwrap();
    let response = get!("/books/");
    let books: Vec<Book> = serde_json::from_str(&response).unwrap();
    //Find the book with the right id
    let book = books.into_iter().find(|b| b.book_id == added_book.book_id).unwrap();
    assert_eq!(book.owned_by.as_str(),"long_isbn_test");
    assert_eq!(book,added_book);
    assert_eq!(book.title.as_str(),"The Art of War");
    let response = get!(format!("/remove_book/{}",book.book_id));
    assert_eq!(response,format!("Deleted book {}",book.book_id));

}


//Test the deletion of books
#[test]
fn delete_book_test() {
    let response = get!("/add_book/0307417131/as/delete_test");
    let added_book: Book = serde_json::from_str(&response).unwrap();
    let response = get!(format!("/remove_book/{}",added_book.book_id));
    assert_eq!(response,format!("Deleted book {}",added_book.book_id));
    let response = get!("/books/");
    let books: Vec<Book> = serde_json::from_str(&response).unwrap();
    //Try to find the book that should have been deleted
    assert!(books.into_iter().find(|b| b.book_id == added_book.book_id).is_none());
}
