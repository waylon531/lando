//    lando, a book management tool
//    Copyright (C) 2018 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#[derive(Serialize,Deserialize,Queryable,PartialEq,Debug)]
pub struct Book {
    pub book_id: BookId,
    pub title: String,
    pub isbn: ISBN,
    pub authors: Vec<String>,
    pub owned_by: String,
    pub borrowed_by: Option<String>,
    pub info_link: String,
    pub description: String,
}

pub type ISBN = i64;
pub type BookId = i32;

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct GoogleBook {
    pub kind: String,
    pub totalItems: i32,
    pub items: Vec<GoogleBookVolume>,
}
#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct GoogleBookVolume {
    pub kind: String,
    pub id: String,
    pub etag: String,
    pub volumeInfo: GoogleBookInfo,
}
#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct GoogleBookInfo {
    pub title: String,
    pub authors: Vec<String>,
    pub description: String,
    pub infoLink: String,
}
