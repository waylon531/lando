CREATE TABLE books (
    id SERIAL PRIMARY KEY,
    title VARCHAR NOT NULL,
    isbn INT NOT NULL,
    authors TEXT[] NOT NULL,
    owned_by TEXT NOT NULL,
    borrowed_by TEXT,
    info_link TEXT NOT NULL,
    description TEXT NOT NULL
)
