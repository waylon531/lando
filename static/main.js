//Wrapper functions around some I already made
//These are here cause I'm adapting them to do stuff with forms
//and I don't want to rewrite them
function processNameForm(e) {
    if (e.preventDefault) e.preventDefault();
    setName();
    return false;
}

function processAddForm(e) {
    if (e.preventDefault) e.preventDefault();
    addBook();
    return false;
}

$(document).ready( function () {
    var form = document.getElementById('name_form');
    if (form.attachEvent) {
        form.attachEvent("submit", processNameForm);
    } else {
        form.addEventListener("submit", processNameForm);
    }


    var form = document.getElementById('add_form');
    if (form.attachEvent) {
        form.attachEvent("submit", processAddForm);
    } else {
        form.addEventListener("submit", processAddForm);
    }
} );

function setName() {
    var name = document.getElementById("name_form").elements[0].value;
    localStorage.setItem("name", name);
    location.reload();
}
function logout() {
    localStorage.removeItem("name");
    location.reload();
}
function addBook() {
    var isbn = document.getElementById("add_form").elements[0].value;
    var name = localStorage.getItem("name");
    $.get(
        "/add_book/{0}/as/{1}".format(isbn,name),
        function( data ) {
            //Reload the page once the book has been borrowed
            location.reload();
        }
    );
}

function borrowBook(bookNumber) {
    var name = localStorage.getItem("name");
    $.get(
        "/borrow/{0}/as/{1}".format(bookNumber,name),
        function( data ) {
            //Reload the page once the book has been borrowed
            location.reload();
        }
    );
}
function returnBook(bookNumber) {
    $.get(
        "/return/{0}".format(bookNumber),
        function( data ) {
            //Reload the page once the book has been borrowed
            location.reload();
        }
    );
}
function removeBook(bookNumber) {
    $.get(
        "/remove_book/{0}".format(bookNumber),
        function( data ) {
            //Reload the page once the book has been borrowed
            location.reload();
        }
    );
}

if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}
